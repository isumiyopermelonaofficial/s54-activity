let collection = [];
let count = 0;

// Write the queue functions below.

function print(){
	return collection;
}

function enqueue(x){
	if(count != 2){
		collection[count] = x;
		count = count + 1;
	} else {
		collection[collection.length] = x;
	}
	console.log(count);
	console.log(collection);
	return collection;
}

function dequeue(){
	let newCollection = [];
	for(let i = 0; i < collection.length - 1; i++){
		newCollection[i] = collection[i+1];
	}
	return collection = [...new Set(newCollection)];
}

function front(){
	return collection[0];
}

function size(){
	return collection.length;
}

function isEmpty(){
	if(collection.length > 0){
		return false;
	} else {
		return true;
	}
}


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};